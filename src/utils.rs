use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

/// Helper function to generate random strings with fixed `length`.
pub fn random_string(length: usize) -> String {
    thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length)
        .map(char::from)
        .collect()
}
