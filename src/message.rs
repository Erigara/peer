use serde::{Deserialize, Serialize};
use std::net::SocketAddr;
use uuid::Uuid;

/// Message represents protocol that peer use to communicate.
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Message {
    /// Used for peer discovery.
    GetAddrs,
    /// Response to GetAddrs. Return peer list of new peers.
    GetAddrsResponse { addrs: Vec<SocketAddr> },
    /// Gossip message with string payload. Broadcasted to all connected peers.
    Gossip {
        id: Uuid,
        payload: String,
        addr: SocketAddr,
    },
}

impl Message {
    pub fn gossip_with_uuid(payload: String, id: Uuid, addr: SocketAddr) -> Self {
        Self::Gossip { id, payload, addr }
    }

    pub fn gossip(payload: String, addr: SocketAddr) -> Self {
        Self::gossip_with_uuid(payload, Uuid::new_v4(), addr)
    }

    pub fn get_addrs() -> Self {
        Self::GetAddrs
    }

    pub fn get_addrs_response(addrs: Vec<SocketAddr>) -> Self {
        Self::GetAddrsResponse { addrs }
    }
}
