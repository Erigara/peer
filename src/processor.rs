use std::{error::Error, net::SocketAddr, sync::Arc};

use tokio::sync::{
    mpsc::{Receiver, Sender},
    Mutex,
};

use crate::{message::Message, state::State, RecvMessage, SendMessage};

/// Implement peer communication protocol.
async fn process_message(
    recv_message: RecvMessage,
    state: Arc<Mutex<State>>,
) -> Option<SendMessage> {
    match recv_message {
        // Broadcast message to all connected peers except recv_from
        (recv_from, Message::Gossip { id, payload, addr }) => {
            // Resend gossip message to peers if not seen earlier
            if !state.lock().await.seen.contains(&id) {
                // Add message to seen to avoid broadcast storm
                state.lock().await.seen.push(id);
                let peers = &state.lock().await.peers;
                let recipients: Vec<SocketAddr> = match recv_from {
                    Some(recv_from) => peers
                        .into_iter()
                        .cloned()
                        .filter(|addr| addr != &recv_from)
                        .collect(),
                    None => peers.into_iter().cloned().collect(),
                };
                match recv_from {
                    Some(recv_from) => {
                        println!(
                            "recieve gossip '{}' from peer {:?} through {:?}",
                            &payload, addr, recv_from
                        );
                    }
                    None => {
                        if !recipients.is_empty() {
                            println!("send gossip '{}' to peers {:?}", &payload, peers);
                        }
                    }
                }
                let message = Message::gossip_with_uuid(payload, id, addr);
                Some((recipients, message))
            } else {
                None
            }
        }
        (Some(recv_from), Message::GetAddrs) => {
            let peers = &mut state.lock().await.peers;
            let addrs: Vec<SocketAddr> = peers
                .iter()
                .cloned()
                .filter(|addr| addr != &recv_from)
                .collect();
            if !peers.contains(&recv_from) {
                peers.insert(recv_from);
                println!("add 1 new peer");
            }
            let message = Message::get_addrs_response(addrs);
            Some((vec![recv_from], message))
        }
        (Some(recv_from), Message::GetAddrsResponse { addrs }) => {
            let message = Message::get_addrs_response(vec![]);
            let peers = &mut state.lock().await.peers;
            // Find new peers: insert them into peers set and add to recipients
            let mut recipients = vec![];
            let mut counter = 0_u32;
            for send_to in addrs {
                if !peers.contains(&send_to) {
                    recipients.push(send_to);
                    peers.insert(send_to);
                    counter += 1;
                }
            }
            if !peers.contains(&recv_from) {
                peers.insert(recv_from);
                counter += 1;
            }
            if counter > 0 {
                println!("add {} new peers", counter);
            }

            Some((recipients, message))
        }
        _ => {
            println!("recieve unexpected message!");
            None
        }
    }
}

/// Component responsible for implementing peer communication protocol.
/// Every received message transformed to new message and/or changes in state.
pub struct MessageProcessor {
    state: Arc<Mutex<State>>,
    // Channel part connected to reciever
    rx: Receiver<RecvMessage>,
    // Channel part conndected to sender
    tx: Sender<SendMessage>,
}

impl MessageProcessor {
    /// Create new `MessageProcessor` instance.
    pub fn new(
        state: Arc<Mutex<State>>,
        rx: Receiver<RecvMessage>,
        tx: Sender<SendMessage>,
    ) -> Self {
        Self { state, rx, tx }
    }

    /// Run `MessageProcessor` instance.
    pub async fn run(self) -> Result<(), Box<dyn Error>> {
        let Self { state, mut rx, tx } = self;
        while let Some(recv_message) = rx.recv().await {
            if let Some(send_message) = process_message(recv_message, state.clone()).await {
                tx.send(send_message).await?;
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use uuid::Uuid;

    use super::*;

    // create state with:
    // - peers: 127.0.0.1:8081, 127.0.0.1:8082, 127.0.0.1:8083, 127.0.0.1:8084
    // - seen: empty
    fn setup_state() -> Arc<Mutex<State>> {
        let mut state = State::new(10);
        // add some peers
        state.peers.extend(vec![
            SocketAddr::from(([127, 0, 0, 1], 8081)),
            SocketAddr::from(([127, 0, 0, 1], 8082)),
            SocketAddr::from(([127, 0, 0, 1], 8083)),
            SocketAddr::from(([127, 0, 0, 1], 8084)),
        ]);
        Arc::new(Mutex::new(state))
    }

    #[tokio::test]
    async fn test_get_addrs() {
        let state = setup_state();
        let recv_message = Message::get_addrs();
        let recv_from = SocketAddr::from(([127, 0, 0, 1], 8085));

        let result = process_message((Some(recv_from), recv_message), state.clone()).await;
        if let Some((send_to, Message::GetAddrsResponse { addrs })) = result {
            assert_eq!(
                send_to,
                vec![recv_from],
                "GetAddrsResponse message must be send to recv_from peer"
            );
            let peers = &state.lock().await.peers;
            // check that addrs is subset of peers
            for addr in &addrs {
                assert!(
                    peers.contains(addr),
                    "GetAddrsResponse must contain only peers"
                );
            }
        } else {
            assert!(false, "GetAddrs must return GetAddrsResponse");
        }
    }

    #[tokio::test]
    async fn test_get_addrs_response() {
        let state = setup_state();
        let recv_from = SocketAddr::from(([127, 0, 0, 1], 8085));
        let addrs = vec![
            SocketAddr::from(([127, 0, 0, 1], 8086)),
            SocketAddr::from(([127, 0, 0, 1], 8087)),
            SocketAddr::from(([127, 0, 0, 1], 8088)),
            SocketAddr::from(([127, 0, 0, 1], 8089)),
        ];
        let recv_message = Message::get_addrs_response(addrs);

        let result = process_message((Some(recv_from), recv_message), state.clone()).await;
        if let Some((send_to, Message::GetAddrsResponse { addrs })) = result {
            assert!(
                addrs.is_empty(),
                "in response to GetAddrsResponse addrs must be empty"
            );
            let peers = &state.lock().await.peers;
            // check that send_to is subset of peers
            for addr in &send_to {
                assert!(
                    peers.contains(addr),
                    "GetAddrsResponse must reply only to peers"
                );
            }
        } else {
            assert!(false, "GetAddrsResponse must return GetAddrsResponse");
        }
    }

    #[tokio::test]
    async fn test_gossip_unchanged_message() {
        let state = setup_state();
        let local_addr = SocketAddr::from(([127, 0, 0, 1], 8080));
        let recv_message = Message::gossip("hello world".to_string(), local_addr);

        let result = process_message((None, recv_message.clone()), state.clone()).await;
        assert!(
            result.is_some(),
            "unseen Gossip message must be broadcasted"
        );
        let (_, send_message) = result.unwrap();
        assert_eq!(
            recv_message, send_message,
            "Gossip message must remain unchanged"
        );
    }

    #[tokio::test]
    async fn test_gossip_broadcast_to_peers() {
        let state = setup_state();
        let local_addr = SocketAddr::from(([127, 0, 0, 1], 8080));
        let recv_message = Message::gossip("hello world".to_string(), local_addr);

        let result = process_message((None, recv_message.clone()), state.clone()).await;
        let (addrs, _) = result.unwrap();
        // Check that peers and addrs have the same elements
        let peers = &state.lock().await.peers;
        for peer in peers {
            assert!(
                addrs.contains(peer),
                "Gossip message must be broadcasted to every peer"
            );
        }
        for addr in &addrs {
            assert!(
                peers.contains(addr),
                "Gossip message must be broadcasted only to peers"
            );
        }
    }

    #[tokio::test]
    async fn test_gossip_broadcast_exclude_recv_from_addr() {
        let state = setup_state();
        let recv_from = SocketAddr::from(([127, 0, 0, 1], 8081));
        let recv_message = Message::gossip("hello world".to_string(), recv_from);

        let result = process_message((Some(recv_from), recv_message.clone()), state.clone()).await;
        let (addrs, _) = result.unwrap();
        assert!(
            !addrs.contains(&recv_from),
            "Gossip message must exclude recv_from addr"
        );
    }

    #[tokio::test]
    async fn test_gossip_already_seen() {
        let state = setup_state();
        let recv_from = SocketAddr::from(([127, 0, 0, 1], 8082));
        let addr = SocketAddr::from(([127, 0, 0, 1], 8081));
        let id = Uuid::new_v4();
        let message = Message::gossip_with_uuid("hello world".to_string(), id, addr);

        // put message id into seen
        state.lock().await.seen.push(id);

        let result = process_message((Some(recv_from), message), state.clone()).await;
        assert!(
            result.is_none(),
            "already seen Gossip messages not broadcasted"
        );
    }
}
