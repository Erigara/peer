use std::{cmp::PartialEq, collections::VecDeque};

/// Wrapper around VecDeque to store message history.
/// Store only last `capacity` messages, old messages are discarded.
pub struct RingBuffer<T: PartialEq> {
    buffer: VecDeque<T>,
    capacity: usize,
}

impl<T: PartialEq> RingBuffer<T> {
    /// Create new `RingBuffer`.
    pub fn new(capacity: usize) -> Self {
        Self {
            buffer: VecDeque::with_capacity(capacity),
            capacity,
        }
    }
    /// Push new element to buffer.
    /// If buffer is full discard oldest element.
    pub fn push(&mut self, value: T) {
        let len = self.buffer.len();
        let capacity = self.capacity;
        if len >= capacity {
            self.buffer.pop_back();
        }
        self.buffer.push_front(value);
    }
    /// Checks if buffer contains value.
    pub fn contains(&mut self, value: &T) -> bool {
        self.buffer.contains(value)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_push_lastest_contains() {
        let mut buffer = RingBuffer::new(5);
        for i in 0..=10 {
            buffer.push(i);
            assert!(
                buffer.contains(&i),
                "latest value must be present in buffer"
            );
        }
    }
}
