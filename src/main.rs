use std::error::Error;

use peer::{
    cliargs::{Args, Parser},
    Peer,
};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    let peer = Peer::new(args).await?;
    peer.run().await?;
    Ok(())
}
