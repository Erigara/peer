use std::time::Duration;
use tokio::{sync::mpsc::Sender, time::sleep};

/// Trait to provide generator for `PeriodicMessageProducer`.
pub trait MessageGenerator<T> {
    fn generate(&self) -> T;
}

/// Generate message every `period` based on provided `generator`.
pub struct PeriodicMessageProducer<T, I>
where
    I: MessageGenerator<T>,
{
    period: Duration,
    generator: I,
    tx: Sender<T>,
}

impl<T, I> PeriodicMessageProducer<T, I>
where
    I: MessageGenerator<T>,
{
    /// Create new `MessageProducer`instance.
    pub fn new(period: Duration, generator: I, tx: Sender<T>) -> Self {
        Self {
            period,
            generator,
            tx,
        }
    }

    /// Run `MessageProducer` instance.
    pub async fn run(self) {
        let Self {
            period,
            generator,
            tx,
        } = self;
        loop {
            let message = generator.generate();
            if let Err(_) = tx.send(message).await {
                break;
            }
            sleep(period).await;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::sync::mpsc;

    // Generator for testing purposes
    struct StringMessageGenerator {
        payload: String,
    }

    impl MessageGenerator<String> for StringMessageGenerator {
        fn generate(&self) -> String {
            self.payload.clone()
        }
    }

    #[tokio::test]
    async fn test_producer() {
        let message = "hello world";
        let (tx, mut rx) = mpsc::channel::<String>(1_000);
        let generator = StringMessageGenerator {
            payload: message.to_string(),
        };
        let p = PeriodicMessageProducer::new(Duration::from_secs(1), generator, tx);

        tokio::spawn(async move { p.run().await });

        assert_eq!(Some(message.to_string()), rx.recv().await)
    }
}
