use std::{error::Error, net::SocketAddr, sync::Arc};

use tokio::{net::UdpSocket, sync::mpsc::Sender};

/// Component responsible for recieving and desirializing messages from other peers.
pub struct MessageReciever<T> {
    tx: Sender<(Option<SocketAddr>, T)>,
    socket: Arc<UdpSocket>,
}

impl<T> MessageReciever<T>
where
    T: serde::de::DeserializeOwned + std::fmt::Debug,
{
    /// Create new `MessageReciever` instance.
    pub fn new(tx: Sender<(Option<SocketAddr>, T)>, socket: Arc<UdpSocket>) -> Self {
        Self { tx, socket }
    }

    /// Run `MessageReciever` instance.
    pub async fn run(self) -> Result<(), Box<dyn Error>> {
        let Self { tx, socket } = self;

        let mut buf = [0; 1024];
        loop {
            let (len, recv_from) = socket.recv_from(&mut buf).await?;
            let message = match bincode::deserialize(&buf[..len]) {
                Ok(message) => message,
                Err(err) => {
                    println!("recieve malformed message from peer {}: {}", recv_from, err);
                    continue;
                }
            };
            if let Err(_) = tx.send((Some(recv_from), message)).await {
                println!("error while sending, aborting...");
                break;
            }
        }
        Ok(())
    }
}
