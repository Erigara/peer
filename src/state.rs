use crate::ringbuffer::RingBuffer;
use std::{collections::HashSet, net::SocketAddr};
use uuid::Uuid;

/// Peer state.
pub struct State {
    /// Set of directly connected peers;
    pub peers: HashSet<SocketAddr>,
    /// Buffer with latest recieved gossip messages ids.
    pub seen: RingBuffer<Uuid>,
}

impl State {
    /// Create new `State` with `capacity` buffer size.
    pub fn new(capacity: usize) -> Self {
        Self {
            peers: HashSet::new(),
            seen: RingBuffer::new(capacity),
        }
    }
}
