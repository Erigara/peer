pub use clap::Parser;
use std::net::SocketAddr;

/// Simple p2p gossiping application
#[derive(Parser, Debug, PartialEq)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// Peers to connect
    #[clap(long, multiple_values = true, multiple_occurrences = false)]
    pub connect: Vec<SocketAddr>,

    /// Addr to bind
    #[clap(long, default_value_t = SocketAddr::from(([0, 0, 0, 0], 8080)))]
    pub bind: SocketAddr,

    /// Send message every `period` seconds
    #[clap(long, default_value_t = 10)]
    pub period: u64,

    /// Message length
    #[clap(long, default_value_t = 10)]
    pub length: usize,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        assert_eq!(
            Args::parse_from(&[
                "peer",
                "--connect",
                "127.0.0.1:8080",
                "--period",
                "20",
                "--bind",
                "0.0.0.0:9000",
                "--length",
                "8",
            ]),
            Args {
                period: 20,
                bind: SocketAddr::from(([0, 0, 0, 0], 9000)),
                connect: vec![SocketAddr::from(([127, 0, 0, 1], 8080))],
                length: 8,
            }
        )
    }
}
