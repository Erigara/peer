pub mod cliargs;
mod message;
mod processor;
mod producer;
mod reciever;
mod ringbuffer;
mod sender;
mod state;
mod utils;
use crate::{
    message::Message,
    processor::MessageProcessor,
    producer::{MessageGenerator, PeriodicMessageProducer},
    reciever::MessageReciever,
    sender::MessageSender,
    state::State,
};
use cliargs::Args;
use std::{error::Error, net::SocketAddr, sync::Arc, time::Duration};
use tokio::{
    net::UdpSocket,
    sync::{
        mpsc::{self, Sender},
        Mutex,
    },
};

/// Shorthand for type passed between MessageReciever and MessageProccessor
type RecvMessage = (Option<SocketAddr>, Message);
/// Shorthand for type passed between MessageProcessor and MessageSender
type SendMessage = (Vec<SocketAddr>, Message);

/// Generator Gossip messages
struct GossipMessageGenerator {
    length: usize,
    addr: SocketAddr,
}

impl MessageGenerator<RecvMessage> for GossipMessageGenerator {
    /// Produce Gossip message with recv_from = None
    fn generate(&self) -> RecvMessage {
        (
            None,
            Message::gossip(utils::random_string(self.length), self.addr),
        )
    }
}

/// Represent peer instance.
///
/// Peer consist of multiple interconected parts:
/// - Producer: produce Gossip messages;
/// - Reciever: recieve and deserialize messages from other peers;
/// - Sender: serialize and send/broadcast messages to other peers;
/// - Processor: implement peer communication protocol: peer discovery, gossip broadcasting ...
///
/// Peers communicates through UDP.
/// Gossip message propagation implemented through flooding.
#[allow(dead_code)]
pub struct Peer {
    sender: MessageSender<Message>,
    reciever: MessageReciever<Message>,
    processor: MessageProcessor,
    producer: PeriodicMessageProducer<RecvMessage, GossipMessageGenerator>,
    socket: Arc<UdpSocket>,
    reciever_tx: Sender<RecvMessage>,
    sender_tx: Sender<SendMessage>,
    peers: Vec<SocketAddr>,
    state: Arc<Mutex<State>>,
}

impl Peer {
    /// Create new peer instance.
    pub async fn new(args: Args) -> Result<Self, Box<dyn Error>> {
        let socket = Arc::new(UdpSocket::bind(&args.bind).await?);
        let state = Arc::new(Mutex::new(state::State::new(100)));

        let (reciever_tx, reciever_rx) = mpsc::channel::<RecvMessage>(1_000);
        let (sender_tx, sender_rx) = mpsc::channel::<SendMessage>(1_000);

        let generator = GossipMessageGenerator {
            length: args.length,
            addr: args.bind,
        };

        let peer = Self {
            sender: sender::MessageSender::new(sender_rx, socket.clone()),
            reciever: reciever::MessageReciever::new(reciever_tx.clone(), socket.clone()),
            processor: processor::MessageProcessor::new(
                state.clone(),
                reciever_rx,
                sender_tx.clone(),
            ),
            producer: producer::PeriodicMessageProducer::new(
                Duration::from_secs(args.period),
                generator,
                reciever_tx.clone(),
            ),
            peers: args.connect,
            socket,
            reciever_tx,
            sender_tx,
            state,
        };

        Ok(peer)
    }

    /// Run peer instance.
    pub async fn run(self) -> Result<(), Box<dyn Error>> {
        let Self {
            sender,
            reciever,
            processor,
            producer,
            socket,
            peers,
            sender_tx,
            ..
        } = self;

        println!("socket addr: {}", &socket.local_addr()?);

        // Send GetAddrs messages to bootstrap peers
        sender_tx.send((peers, Message::get_addrs())).await?;

        tokio::spawn(async move {
            reciever.run().await.unwrap();
        });
        tokio::spawn(async move {
            sender.run().await.unwrap();
        });
        tokio::spawn(async move {
            processor.run().await.unwrap();
        });
        producer.run().await;

        Ok(())
    }
}
