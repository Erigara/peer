use std::{error::Error, net::SocketAddr, sync::Arc};

use tokio::{net::UdpSocket, sync::mpsc::Receiver};

/// Component responsible for serializing and sending messages to connected peers.
pub struct MessageSender<T> {
    rx: Receiver<(Vec<SocketAddr>, T)>,
    socket: Arc<UdpSocket>,
}

impl<T> MessageSender<T>
where
    T: serde::ser::Serialize,
{
    /// Create new `MessageSender` instance.
    pub fn new(rx: Receiver<(Vec<SocketAddr>, T)>, socket: Arc<UdpSocket>) -> Self {
        Self { rx, socket }
    }

    /// Run `MessageSender`.
    pub async fn run(self) -> Result<(), Box<dyn Error>> {
        let Self { mut rx, socket } = self;

        while let Some((send_to, message)) = rx.recv().await {
            let encoded = bincode::serialize(&message)?;
            for addr in &send_to {
                socket.send_to(&encoded, addr).await?;
            }
        }
        Ok(())
    }
}
