# Peer

Simple p2p gossiping application.

## Installation

```bash
cargo install --git https://gitlab.com/Erigara/peer.git
```

## Usage

Run peer:

```
peer [--bind 127.0.0.1:8080] [--connect 127.0.0.1:8081 127.0.0.1:8082] [--period 7] [--length 10]
```

Where:

- `bind`: socket address for peer;
- `connect`: list of peer`s socket addresses separated by whitespace;
- `period`: generate gossip message every `peer` seconds;
- `length`: gossip message length;

Show help message:

```bash
peer --help
```